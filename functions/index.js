/* eslint-disable promise/always-return */
const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
const moment = require('moment')
admin.initializeApp()
const db = admin.firestore()

const getProducts = require('./products/get')

// Write products to the database, overwriting the existing ones
exports.writeProducts = functions.https.onRequest(async (req, res) => {
  return cors(req, res, async () => {
    // Valida que la peticion sea POST
    if (req.method !== 'POST') {
      return res.status(405).send('Metodo no disponible')
    }
    // Inicializa variables
    console.info('Recibida peticion de escritura de products')
    const serverTime = moment.utc().format()
    const batch = db.batch()
    const categories = []
    // Recorrido por cada product recibido en la peticion
    req.body.forEach(product => {
      // Se agrega el departamento al arreglo de categorias
      product.category = String(product.category).replace(' ', '-')
      if (!categories.includes(product.category)) {
        categories.push(product.category)
      }
      // Se prepara el objeto product que se va a insertar
      const code = product.code
      delete product.code
      product.lastUpdated = serverTime
      // Se agregan las insersiones al batch
      batch.set(db.doc(`products/${code}`), product)
    })
    const categoryDefault = { enabled: true }
    categories.forEach(category => {
      console.log(`Categories: ${category}`)
      batch.set(db.doc(`categories/${category}`), categoryDefault, { mergeFields: ['enabled'] })
    })
    // Se ejecuta el batch y en caso de error se muestra en bitacora y retorna el error
    return batch.commit()
      .then(result => {
      // Si no hay errores se retorna un mensaje de exito
        console.log(result)
        return res.status(200).send('Products escritos')
      })
      .catch(err => {
        console.log(JSON.stringify(err.message))
        return res.status(400).send('Error al insertar products')
      })
  })
})

// Get the list of products
exports.getProducts = require('./products/get')

// Update the products information using transactions for consistency
exports.updateProducts = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    // Valida que la peticion sea POST
    if (req.method !== 'PUT') {
      return res.status(405).send('Metodo no disponible')
    }
    // Inicializa variables
    console.info('Recibida peticion de actualizacion de products')
    const serverTime = moment.utc().format()
    var errors = []
    var success = []
    // Recorrido por cada product recibido en la peticion
    req.body.forEach(product => {
      // Se ejecuta una transaccion para actualizar el stock del product y el precio
      console.log(`Actualizando el product ${product.codigo}`)
      db.runTransaction(transaction => {
        var docRef = db.doc(`catalog/${product.codigo}`)
        return transaction.get(docRef).then(doc => {
          const existencia = product.existencia + doc.data().existencia
          transaction.update(docRef, {
            precio: product.precio,
            existencia: existencia,
            lastUpdated: serverTime,
            activo: product.activo,
            departamento: product.categoryo.replace(' ', '-'),
            descripcion: product.descripcion
          })
        })
      }).then(result => {
        console.log(`Exito en transaccion: ${JSON.stringify(result)}`)
        success.push(`Exito en transaccion: ${JSON.stringify(result)}`)
      }).catch(err => {
        console.log(`Error en transaccion del product ${product.codigo}: ${err.message}`)
        errors.push(`Error en transaccion del product ${product.codigo}: ${err.message}`)
      })
    })
    if (errors) {
      return res.status(400).send('Errores en las transacciones de actualizacion de products')
    }
    // Si no hay errores se retorna un mensaje de exito
    return res.status(200).send('Products actualizados')
  })
})

/*
exports.updateDepartment = functions.firestore.document('catalogo/{idproduct}/').onWrite((change, context) => {
  const doc = change.after.data()
  const old = change.before.data()
  console.log(context.eventType)
  // Si no ha habido cambio en el departamento se termina la funcion
  if (doc.departamento === old.departamento) {
    return null
  }
  const batch = db.batch()
  var departamentos = []
  const idproduct = context.params.idproduct
  if (!doc) {
    return null
  }
  // Se busca el product en los departamentos y se eliminan
  // Primero se hace un arreglo con todos los departamentos para evitar promesas anidadas
  db.collection('departamento').get().then(dataSnapshot => {
    dataSnapshot.forEach(departamento => {
      departamentos.push(departamento)
    })
  }).catch(error => {
    console.log(error.message)
  })
  // Luego se recorre cada uno de estos departamentos para ver si contienen el product y agregarlo al batch para eliminarse
  departamentos.forEach(departamento => {
    db.doc(`${departamento.ref.path}/${idproduct}`).get().then(product => {
      if (product.exists) {
        batch.delete(product.ref.path)
      }
    }).catch(error => {
      console.log(error.message)
    })
  })
  // Se elimina el product de los departamentos
  batch.commit().catch(error => {
    console.log(`Error al eliminar los products de los departamentos: ${error.message}`)
  })
  // Se preparan los datos para guardar en la coleccion de departamentos
  const catProduct = {
    doc: change.after.ref,
    descripcion: doc.descripcion,
    precio: doc.precio
  }
  // Se guarda la informacion del product en la categoria
  return db.doc(`departamento/${doc.departamento}/products/${context.params.idproduct}`).set(catProduct).then(data => {
    console.info(`Actualizada la categoria del product ${data.descripcion}`)
  }).catch(error => {
    console.error(`Error al actualizar la categoria del product ${doc.codigo}: ${error.message}`)
  })
})
*/
