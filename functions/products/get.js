/* eslint-disable promise/always-return */
const functions = require('firebase-functions')
const db = require('firebase-admin').initializeApp().firestore()
const cors = require('cors')({
  origin: true
})

exports.getProducts = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    // Valida que la peticion sea GET
    if (req.method !== 'GET') {
      return res.status(405).send('Metodo no disponible')
    }
    return db.collection('products').get().then(snapshot => {
      const datos = snapshot.docs.map(doc => {
        var newDoc = doc.data()
        newDoc.codigo = doc.id
        return newDoc
      })
      return res.status(200).send(JSON.stringify(datos))
    }).catch(error => {
      console.log(`Error al consultar products: ${error.message}`)
      return res.status(404).send('Error en la consulta de datos')
    })
  })
})
