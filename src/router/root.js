export default [
  {
    path: '/',
    name: 'Categories',
    component: () => import('../views/shop/Categories.vue')
  },
  {
    path: '/cat/:category',
    name: 'Products',
    component: () => import('../views/shop/Products.vue'),
    props: true
  },
  {
    path: '/checkout',
    name: 'Checkout',
    component: () => import('../views/shop/Checkout.vue'),
    props: true,
    meta: {
      loginRequired: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/user/Profile.vue'),
    props: true,
    meta: {
      loginRequired: true
    }
  },
  {
    path: '*',
    name: 'Default',
    component: () => import('../views/shop/Categories.vue')
  }
]
