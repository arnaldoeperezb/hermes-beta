import { db } from '../../firebaseApp/firestore'

export default {
  mounted () {
    db.collection('categories').where('enabled', '==', true).onSnapshot(snapshot => {
      this.categories = snapshot.docs.map(data => {
        var doc = data.data()
        doc.title = data.id.replace('-', ' ')
        if (!doc.img || doc.img === 'default') doc.img = require('../../assets/categoria.png')
        doc.url = `/cat/${data.id}`
        doc.id = data.id
        return doc
      })
    })
  },
  data () {
    return {
      categories: []
    }
  }
}
