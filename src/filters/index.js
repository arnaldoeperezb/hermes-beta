import Vue from 'vue'

export default () => {
  Vue.filter('currency', (value) => {
    const options = { style: 'currency', currency: 'VEF' }
    const numberFormat = new Intl.NumberFormat('es-VE', options)
    return numberFormat.format(Number.parseFloat(value))
  })
}
